$(document).ready(function() {
	var mineFieldWidth;
	var mineFieldHeight;
	var mineAmount;
    var mineCount;
    var mineLocations;
    var flagCount;
    var numberOfMinesFlagged;
	
	$("#buttonCreate").click(function() {
        //clearing the mineField of singleFields when re-creating the mineField
		$(".singleField").remove();
        mineLocations = null;
        flagCount = 0;
        numberOfMinesFlagged = 0;
		
        //reading out the input fields and checking for numbers out of logical bounds
		mineFieldWidth = $("#inputWidth").val();
		mineFieldHeight = $("#inputHeight").val();
		mineAmount = $("#inputMineAmount").val();
		
		if(mineFieldWidth < 1) mineFieldWidth = 2;
		if(mineFieldHeight < 1) mineFieldHeight = 2;
		
		if(mineAmount >= mineFieldWidth*mineFieldHeight) mineAmount = mineFieldWidth*mineFieldHeight-1;
		if(mineAmount < 1) mineAmount = 1;
        mineCount = mineAmount;
		
        //changing the mine Fields max width depending on the given mineFieldWidth and the size of singleField
		$("#mineField").css("width", mineFieldWidth*24+"px"); //12: 20px breite + 1px rand links + 1px rand rechts $("#mineField").css("width")+($("#mineField").css("border")*2)
		
        //placement of singleFields according to the given mineField width and height
        mineLocations = createArray(mineFieldWidth, mineFieldHeight);
                 
        for(var y=0;y<mineFieldHeight;y++) {
            for(var x=0;x<mineFieldWidth;x++) {       
				$("#mineField").append("<div data-position-y='"+y+"' data-position-x='"+x+"' data-is-revealed='false' data-is-flagged='false' data-adjacent-mines='0' class='singleField'></div>");
                mineLocations[x][y] = false;
			}
		}
        
        //random placement of mines
		while(mineCount > 0) {		
            var randomX = Math.floor(Math.random() * mineFieldWidth);
            var randomY = Math.floor(Math.random() * mineFieldHeight);
            
            if(!mineLocations[randomX][randomY]) {
                mineLocations[randomX][randomY] = true;
                mineCount--;
            }
		}
        
        //calculation of the adjacent mines to be revealed on click
        for(var y=0;y<mineFieldHeight;y++) {
            for(var x=0;x<mineFieldWidth;x++) {
                checkSurroundings(x, y);
            };
        };
        
	});
	
	
	$("#mineField").on("click", ".singleField", function() {
        var x = parseInt($(this).attr("data-position-x"), 10);
        var y = parseInt($(this).attr("data-position-y"), 10);
        
        if(!$(this).data("is-flagged")) {
            if(!mineLocations[x][y]) {
                if($(this).data("adjacent-mines") == 0) {
                    revealFields(x, y);                   
                }
                else {
                    revealSingleField(x, y);
                }
            } else {
                for(var y=0;y<mineFieldHeight;y++) {
                    for(var x=0;x<mineFieldWidth;x++) {
                        if(mineLocations[x][y]) {
                            $(".singleField[data-position-x='"+x+"'][data-position-y='"+y+"']").css("background-color", "red");   
                        }
                    }
                }
                alert("GAME OVER!");
            }
        }
        if(numberOfMinesFlagged == mineAmount) checkWinningConditions();
	});
    
    $("#mineField").on("contextmenu", ".singleField", function() {
        if($(this).data("is-revealed") == false) {
            if($(this).data("is-flagged") == false) {
                if(flagCount < mineAmount) {
                    $(this).css("background-color", "yellow");
                    $(this).attr("data-is-flagged", true);
                    $(this).data("is-flagged", true);
                    flagCount++;
                    if(mineLocations[$(this).data("position-x")][$(this).data("position-y")]) numberOfMinesFlagged++;
                }
            } 
            else {
                $(this).css("background-color", "lightgray");
                $(this).attr("data-is-flagged", false);
                $(this).data("is-flagged", false);
                flagCount--;
                if(mineLocations[$(this).data("position-x")][$(this).data("position-y")]) numberOfMinesFlagged--;
            }
        }
        if(numberOfMinesFlagged == mineAmount) checkWinningConditions();
    });
    
    function checkWinningConditions() {        
        var nrOfFieldsRevealed = 0;
        
        for(var y=0;y<mineFieldHeight;y++) {
            for(var x=0;x<mineFieldWidth;x++) {
                if($(".singleField[data-position-x='"+x+"'][data-position-y='"+y+"']").data("is-revealed")) {
                    nrOfFieldsRevealed++;
                }
            }
        }
        if(nrOfFieldsRevealed == (mineFieldHeight*mineFieldWidth)-mineAmount) alert("Congratulation! You won!");
    }
    
    function revealSingleField(x, y) {
        var fieldToReveal = $(".singleField[data-position-x='"+x+"'][data-position-y='"+y+"']");
        
        if(!isFlagged(x, y)) {
            if(!mineLocations[x][y]) {
                $(fieldToReveal).data("is-revealed", true);
                $(fieldToReveal).css("background-color", "gray");

                if($(fieldToReveal).attr("data-adjacent-mines") != 0) {
                    $(fieldToReveal).find("span").remove();
                    $(fieldToReveal).append("<span>"+$(fieldToReveal).attr("data-adjacent-mines")+"</span>");
                }
            }
        }
    };
    
    function isFlagged(x, y) {
        return $(".singleField[data-position-x='"+x+"'][data-position-y='"+y+"']").data("is-flagged");
    }
    
    function canReveal(x, y) {
        if(!$(".singleField[data-position-x='"+x+"'][data-position-y='"+y+"']").data("is-revealed") && $(".singleField[data-position-x='"+x+"'][data-position-y='"+y+"']").data("adjacent-mines") >= 0 && !mineLocations[x][y])         {
            return true;   
        } 
        else return false;
    }
    
    function revealFields(x, y) {
        var left = x-1;
        var right = x+1;
        var top = y-1;
        var bottom = y+1;
        
        revealSingleField(x, y);
        
        if($(".singleField[data-position-x='"+x+"'][data-position-y='"+y+"']").data("adjacent-mines") == 0) {
           if(left >= 0 && canReveal(left, y)) revealFields(left, y);
            if(right < mineFieldWidth && canReveal(right, y)) revealFields(right, y);
            if(top >= 0 && canReveal(x, top)) revealFields(x, top);
            if(bottom < mineFieldHeight && canReveal(x, bottom)) revealFields(x, bottom); 
        }
    }
    
    function checkSurroundings(x, y) {  
        for(var j=-1;j<2;j++) {
            for(var i=-1;i<2;i++) {
                if(x+i >= 0 && x+i < mineFieldWidth && y+j >= 0 && y+j < mineFieldHeight) {
                    if(mineLocations[x+i][y+j]) {
                        var temp = $(".singleField[data-position-x='"+x+"'][data-position-y='"+y+"']");   
                        temp.attr("data-adjacent-mines", parseInt(temp.attr("data-adjacent-mines"), 10)+1);
                    }
                }
            }
        }
    };
    
    function createArray(length) {
        var arr = new Array(length || 0), i = length;

        if (arguments.length > 1) {
            var args = Array.prototype.slice.call(arguments, 1);
            while(i--) arr[length-1 - i] = createArray.apply(this, args);
        }
        return arr;
    };
});